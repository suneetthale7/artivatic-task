import "./App.css";
import 'bootstrap/dist/css/bootstrap.min.css';
import AllStates from "./components/AllStates";

function App() {
  return (
    <div className="App">
      <AllStates />
    </div>
  );
}

export default App;
