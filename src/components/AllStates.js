import React, { Component } from "react";
import Districts from "./Districts";
import GoogleMaps from "./GoogleMaps";

var axios = require("axios");

class AllStates extends Component {
  constructor(props) {
    super(props);

    this.childHandlerDistricts = this.childHandlerDistricts.bind(this);

    this.state = {
      allStatesData: [],
      allDistricts: [],
      selectedState: "",
      selectedDistrict: "",
    };
  }

  componentDidMount = () => {
    setTimeout(() => {
      axios
        .get("./states-and-districts.json")
        .then((object) => {
          return this.setState({ allStatesData: object.data.states });
        })
        .catch((err) => {
          console.log(err.data);
        });
    }, 100);
  };

  loadDistricts = (e) => {
    var clickedState = e.target.value;
    this.setState({ selectedState: clickedState });
    if (clickedState === "Select State") {
      this.setState({ allDistricts: [] });
    }
    this.state.allStatesData.forEach((element) => {
      if (clickedState === element.state) {
        return this.setState({ allDistricts: element.districts });
      }
    });
  };

  childHandlerDistricts(dataFromDistricts) {
    this.setState({ selectedDistrict: dataFromDistricts });
  }

  render() {
    let map;
    if (
      this.state.selectedState &&
      this.state.selectedState !== "Select State"
    ) {
      map = (
        <GoogleMaps
          state={this.state.selectedState}
          districts={this.state.allDistricts}
        />
      );
    }

    return (
      <React.Fragment>
        <div className="container ">
          <div className="row">
            <div className="col-lg-6 offset-lg-3 col-md-12 statescol">
              <form>
                <div className="form-group">
                  <label htmlFor="allStates">Select State</label>
                  <select
                    onChange={this.loadDistricts}
                    className="form-control"
                    id="allStates"
                  >
                    <option value="Select State">Select State</option>
                    {this.state.allStatesData.map((element) => {
                      return (
                        <option key={element.state}>{element.state}</option>
                      );
                    })}
                  </select>
                </div>
              </form>
            </div>
          </div>
          <div className="row">
            <div className="col-lg-6 col-md-12 boxes">
              <Districts
                action={this.childHandlerDistricts}
                eachState={this.state.allDistricts}
              />
            </div>
            <div className="col-lg-6 col-md-12 boxes">
              <h4>Google Maps</h4>
              {map}
              </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default AllStates;
