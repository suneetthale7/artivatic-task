import React, { Component } from "react";
import { Map, Marker, GoogleApiWrapper } from "google-maps-react";
import Geocode from "react-geocode";
import { Spinner } from "react-bootstrap";

Geocode.setApiKey("AIzaSyCGdjXjzj1IekZP8PLkB43uDXqFYLfJ43s");
Geocode.setLanguage("en");
Geocode.setRegion("es");

export class GoogleMaps extends Component {
  constructor(props) {
    super(props);
    this.state = {
      geoLocations: [],
      myLocation: {
        lattitude: "",
        longitude: "",
      },
    };
  }

  componentDidMount() {
    this.getDistrictCoordinates(this.props).then((res) => {
      this.setState({ geoLocations: res });
    });

    // get your location
    navigator.geolocation.getCurrentPosition((position) => {
      this.setState({
        myLocation: {
          lattitude: position.coords.latitude,
          longitude: position.coords.longitude,
        },
      });
    });
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ geoLocations: [] });
    this.getDistrictCoordinates(nextProps).then((res) => {
      this.setState({ geoLocations: res });
    });

    // get your location
    navigator.geolocation.getCurrentPosition((position) => {
      this.setState({
        myLocation: {
          lattitude: position.coords.latitude,
          longitude: position.coords.longitude,
        },
      });
    });
  }

  async getDistrictCoordinates(props) {
    if (props.state !== "" && props.state !== "Select State") {
      const response = await Promise.all(
        props.districts.map(async (dist) => {
          return await Geocode.fromAddress(`${dist}`);
        })
      );

      return response.map((res) => {
        return res.results[0].geometry.location;
      });
    }
    return [];
  }

  render() {
    const { geoLocations, myLocation } = this.state;
    const currentLocation = {
      lat: myLocation.lattitude,
      lng: myLocation.longitude,
    };

    return (
      <div>
          {geoLocations && geoLocations.length ? (
            <Map
              google={this.props.google}
              style={{ width: "100%", height: "100%", position: "relative" }}
              className={"map"}
              initialCenter={currentLocation}
              zoom={4}
            >
              {geoLocations.map((location, idx) => {
                const loc = {
                  lat: location.lat,
                  lng: location.lng,
                };
                return <Marker position={loc} key={idx} />;
              })}
            </Map>
          ) : (
            <Spinner  animation="border" variant="warning" />
          )}

      </div>
    );
  }
}

export default GoogleApiWrapper({
  apiKey: "AIzaSyCGdjXjzj1IekZP8PLkB43uDXqFYLfJ43s",
})(GoogleMaps);
