import React, { Component } from "react";

class Districts extends Component {
  render() {
    return (
      <React.Fragment>
        <div className="cardDistricts">
          <h4>All Districts</h4>
          <div className="list-group districtbtn districtbox scrollbar scrollbar-primary">
            {this.props.eachState.map((data, index) => {
              return (
                <button
                  value={data}
                  className="btn btn-warning districtbtn"
                  key={index}
                >
                  {data}
                </button>
              );
            })}
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default Districts;
